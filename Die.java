import java.util.Random;
public class Die{
	private int faceValue;
	private Random randGen;
	
	public Die(){
		this.faceValue=1;
		Random randGen = new Random();
	}
	
	public int getFaceValue(){
		return this.faceValue;
	}
	
	public void roll(){
		Random randGen = new Random();
		faceValue = randGen.nextInt(6)+1;
		
	}
	
	public String toString(){
		return "Face Value is " +faceValue;
	}
}