public class Jackpot{
	public static void main (String[]args){
		System.out.println("Welcome To Shut In The Box- Thai Variant!");
		Board shutTheBox = new Board();
		boolean gameOver=false;
		int numOfTilesClosed = 0;
		
		while(gameOver==false){
			System.out.println(shutTheBox);
			gameOver = shutTheBox.playATurn();
			if(gameOver==false){
				numOfTilesClosed++;
			}
		}
		if(numOfTilesClosed>=7){
			System.out.println("Congratulations You Reached The Jackpot! You Won with " +numOfTilesClosed+ " Tiles Closed!"); 
		}
		
		else{
			System.out.println("You Lost! You had: " +numOfTilesClosed +" Tiles Closed! Better Luck Next Time!");
		}
	}
}