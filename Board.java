public class Board{
	private Die dieOne;
	private Die dieTwo;
	private boolean[] tiles;
	
	public Board(){
		this.dieOne = new Die();
		this.dieTwo= new Die();
		tiles= new boolean[12];
	}
	public String toString(){
		String stringRepresentationOfTiles="";
		for (int i=0; i<tiles.length; i++){
			if(tiles[i]==true){
				stringRepresentationOfTiles += "X ";
			}
			else{
				int num=i+1;
				stringRepresentationOfTiles+= num+" " ;
			}
		}
		return stringRepresentationOfTiles;
	}
	public boolean playATurn(){
		dieOne.roll();
		dieTwo.roll();
		System.out.println(dieOne);
		System.out.println(dieTwo);
		int sumOfDice= dieOne.getFaceValue() + dieTwo.getFaceValue();
		if(tiles[sumOfDice-1]==false){
			tiles[sumOfDice-1]=true;
			System.out.println("Closing tile equal to sum: " +sumOfDice);
			return false;
		}
		else if(tiles[dieOne.getFaceValue()-1]==false){
			tiles[dieOne.getFaceValue()-1]=true;
			System.out.println("Closing tile with the same value as Die One: " +dieOne.getFaceValue());
			return false;
		}			
		else if(tiles[dieTwo.getFaceValue()-1]==false){
			tiles[dieTwo.getFaceValue()-1]=true;
			System.out.println("Closing tile with the same value as Die Two: " +dieTwo.getFaceValue());
			return false;
		}
		else{
			System.out.println("All the tiles for these values are already shut!");
			return true;
		}
	}
}